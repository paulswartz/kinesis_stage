#!/usr/bin/env python
"""
Script to generate JSON CloudEvents from a OCS message logfile.

The output is one JSON line per OCS message line.

Usage:

$ python ocs-as-cloudevent-json-batched.py < ocs-messages.txt > cloudevents.jl
"""
import sys
import json
from hashlib import blake2b
from datetime import datetime
import pytz
from collections import Counter
import zlib
import os
import base64
import itertools

"""
example cloudevent, from https://github.com/cloudevents/spec/blob/master/spec.md:

{
    "specversion" : "1.0",
    "type" : "com.github.pull_request.opened",
    "source" : "https://github.com/cloudevents/spec/pull",
    "subject" : "123",
    "id" : "A234-1234-1234",
    "time" : "2018-04-05T17:31:00Z",
    "comexampleextension1" : "value",
    "comexampleothervalue" : 5,
    "datacontenttype" : "text/xml",
    "data" : "<much wow=\"xml\"/>"
}

example OCS message:
02/11/20,02:00:08,5247,TSCH,02:00:07,R,NEW,9805F8E6,S,R,19:00,19:39,S931_,ALEWIFE,ASHMONT,9805F8E5,9805F8E7
"""

EASTERN = pytz.timezone("America/New_York")
DIGEST_SIZE = 16

def groupby_key(parts):
    """
    Generate a key to be used by itertools.groupby for batching.

    All messages are batched by date/time/type/line. TMOV messages are also
    grouped by vehicle ID to ensure that they are processed more rapidly.
    """
    event_date = parts[0]
    event_time = parts[1]
    message_type = parts[3]
    line = parts[5]
    key = [event_date, event_time, message_type, line]
    if message_type == "TMOV":
        key.append(parts[6]) # vehicle
    return key

def messages(lines):
    split_lines = (line.strip().split(",") for line in lines)
    for (key, grouped_parts) in itertools.groupby(split_lines, groupby_key):
        event_dt = EASTERN.localize(datetime.strptime(f"{key[0]} {key[1]}", "%m/%d/%y %H:%M:%S"))
        id_value = "-".join(key)
        digest = blake2b(id_value.encode('ascii'), digest_size=DIGEST_SIZE)
        messages = []
        for parts in grouped_parts:
            data = ",".join(parts[2:])
            digest.update(data.encode('ascii'))

            [event_date, event_time, sequence_id, message_type, message_time, line] = parts[:6]
            message_dt = EASTERN.localize(datetime.strptime(f"{event_date} {message_time}", "%m/%d/%y %H:%M:%S"))
            data = {
                "received_time": message_dt.isoformat(),
                "raw": data
            }
            messages.append(data)
        print(key, len(messages), file=sys.stderr)
        yield {
            "specversion": "1.0",
            "type": f"com.mbta.ocs.raw-message-batch",
            "source": "opstech3.mbta.com/socketproxy",
            "id": digest.hexdigest(),
            #"id": base64.b64encode(h.digest()).decode('ascii').rstrip("="),
            #"id": data,
            #"id": id_value,
            #"sequence": sequence_id,
            "partitionkey": f"{key[2]}-{key[3]}",
            "time": event_dt.isoformat(),
            #"datacontenttype": "text/csv",
            "data": {
                "size": len(messages),
                "batch": messages
            }
        }


for m in messages(sys.stdin):
    json_dump = json.dumps(m, separators=(',', ':'))
    print(json_dump)
