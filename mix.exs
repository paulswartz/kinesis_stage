defmodule KinesisStage.MixProject do
  use Mix.Project

  def project do
    [
      app: :kinesis_stage,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {KinesisStage.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:gen_stage, "> 0.0.0"},
      {:hackney, "> 0.0.0"},
      {:mint, "> 1.0.0"},
      {:castore, "> 0.0.0"},
      {:event_stream, "> 0.0.0"},
      {:jason, "> 0.0.0"},
      {:ex_aws, "> 0.0.0"},
      {:ex_aws_kinesis, "> 0.0.0"},
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
