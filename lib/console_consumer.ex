defmodule ConsoleConsumer do
  @moduledoc """
  GenStage consumer which logs data to stdout.
  """
  use GenStage

  def start_link(opts) do
    GenStage.start_link(__MODULE__, opts)
  end

  @impl GenStage
  def init(opts) do
    {:consumer, [], opts}
  end

  @impl GenState
  def handle_events(events, _from, delays) do
    now = System.system_time(:second)
    new_delays = for event <- events, do: now - event.approximate_arrival_timestamp
    delays = delays ++ new_delays
    average = Enum.sum(delays) / length(delays)
    IO.inspect({:events, length(events), average})
    {:noreply, [], delays}
  end
end
