defmodule KinesisStage.SubscribeToShard do
  @moduledoc """
  Module implementing the enhanced fan-out API.
  """
  require Mint.HTTP
  defstruct [:conn, :request_ref, :consumer_arn, :shard_id, :iterator_type]

  @namespace "Kinesis_20131202"

  def new(consumer_arn, shard_id, iterator_type) do
    %__MODULE__{
      consumer_arn: consumer_arn,
      shard_id: shard_id,
      iterator_type: iterator_type
    }
  end

  def subscribe(%__MODULE__{} = s) do
    op =
      ExAws.Operation.JSON.new(:kinesis, %{
        data: %{
          "ConsumerARN" => s.consumer_arn,
          "ShardId" => s.shard_id,
          "StartingPosition" => %{
            "Type" => String.upcase(Atom.to_string(s.iterator_type))
          }
        },
        headers: [
          {"x-amz-target", "#{@namespace}.SubscribeToShard"},
          {"content-type", "application/x-amz-json-1.1"}
        ]
      })

    config = ExAws.Config.new(op.service)
    body = config[:json_codec].encode!(op.data)
    url = ExAws.Request.Url.build(op, config)
    uri = URI.parse(url)
    {:ok, headers} = ExAws.Auth.headers(op.http_method, url, op.service, config, op.headers, body)
    method = op.http_method |> Atom.to_string() |> String.upcase()

    {:ok, conn} =
      Mint.HTTP.connect(
        String.to_existing_atom(uri.scheme),
        uri.host,
        uri.port
      )

    {:ok, conn, request_ref} = Mint.HTTP.request(conn, method, uri.path, headers, body)
    %{s | conn: conn, request_ref: request_ref}
  end

  defguard is_connection_message(s, message) when Mint.HTTP.is_connection_message(s.conn, message)

  def stream(%__MODULE__{conn: conn, request_ref: ref} = s, message) do
    {:ok, conn, responses} = Mint.HTTP.stream(conn, message)
    s = %{s | conn: conn}
    if responses != [], do: IO.inspect(responses)

    packets =
      for {:data, ^ref, data} <- responses,
        {:ok, headers, json} = EventStream.decode!(data),
        IO.inspect({headers, json}),
          event_type = :proplists.get_value(":event-type", headers),
          event_type == "SubscribeToShardEvent" do
        Jason.decode!(json)
      end

    if Enum.find(responses, &elem(&1, 0) == :done) do
      IO.inspect(:DISCONNECTING)
      {:ok, _conn} = Mint.HTTP.close(conn)
      {:error, :done, packets}
    else
      {:ok, s, packets}
    end
  end
end
