defmodule KinesisStage.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: KinesisStage.Worker.start_link(arg)
      # {KinesisStage.Worker, arg}
      KinesisStage.ShardSupervisor,
      {KinesisStage, Application.get_all_env(:kinesis_stage)},
      {ConsoleConsumer, subscribe_to: [{KinesisStage, max_demand: 100_000}]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: KinesisStage.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
