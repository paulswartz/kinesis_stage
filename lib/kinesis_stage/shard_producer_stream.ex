defmodule KinesisStage.ShardProducerStream do
  @moduledoc """
  Producer which streams data from a single Kinesis shard.
  """
  use GenStage
  require KinesisStage.SubscribeToShard

  def start_link({shard_id, name, opts}) do
    GenStage.start_link(__MODULE__, {shard_id, opts}, name: name)
  end

  defstruct [
    :stream_name,
    :shard_id,
    :iterator_type,
    :ss,
    demand: 0
  ]

  @impl GenStage
  def init({shard_id, opts}) do
    {:producer,
     %__MODULE__{
       stream_name: Keyword.fetch!(opts, :stream_name),
       shard_id: shard_id,
       iterator_type: Keyword.get(opts, :shard_iterator_type, :trim_horizon)
     }}
  end

  @impl GenStage
  def handle_demand(demand, state) do
    IO.inspect({:new_demand, state.shard_id, demand})
    state = %{state | demand: demand + state.demand}
    state = ensure_ss(state)
    {:noreply, [], state}
  end

  @impl GenStage
  def handle_info(message, state)
      when KinesisStage.SubscribeToShard.is_connection_message(state.ss, message) do
    case KinesisStage.SubscribeToShard.stream(state.ss, message) do
      {:ok, ss, packets} ->
        state = %{state | ss: ss}
        records = Enum.flat_map(packets, &Map.get(&1, "Records", []))
        {state, records} = parse_records(state, records)
        {:noreply, records, state}
      {:error, :done, packets} ->
        state = %{state | ss: nil}
        records = Enum.flat_map(packets, &Map.get(&1, "Records", []))
        {state, records} = parse_records(state, records)
        if state.demand > 0 do
          {:noreply, demand_events, state} = handle_demand(0, state)
          {:noreply, records ++ demand_events, state}
        else
          {:noreply, records, state}
        end
    end
  end

  defp ensure_ss(%{ss: %{}} = state) do
    state
  end

  defp ensure_ss(state) do
    arn = Application.get_env(:kinesis_stage, :consumer_arn)
    ss = KinesisStage.SubscribeToShard.new(arn, state.shard_id, state.iterator_type)
    ss = KinesisStage.SubscribeToShard.subscribe(ss)
    %{state | ss: ss}
  end

  def parse_records(state, records) do
    records =
      for record <- records do
        %{
          "Data" => data,
          "ApproximateArrivalTimestamp" => approximate_arrival,
          "PartitionKey" => partition_key,
          "SequenceNumber" => sequence_number
        } = record

        %KinesisRecord{
          stream_name: state.stream_name,
          shard_id: state.shard_id,
          data: :base64.decode(data),
          approximate_arrival_timestamp: approximate_arrival,
          partition_key: partition_key,
          sequence_number: sequence_number
        }
      end

    demand = max(0, state.demand - length(records))
    if records != [], do: IO.inspect({:sent, state.shard_id, length(records), remaining: demand})
    state = %{state | demand: demand}
    {state, records}
  end
end
