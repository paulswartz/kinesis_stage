defmodule KinesisStage.ShardSupervisor do
  @moduledoc """
  DynamicSupervisor responsible for creating ShardProducer children.
  """
  require Logger

  def child_spec(opts) do
    name = Keyword.get(opts, :name, __MODULE__)
    Supervisor.child_spec({DynamicSupervisor, strategy: :one_for_one, name: name}, [])
  end

  def create_children(name \\ __MODULE__, shard_ids, opts) do
    for shard_id <- shard_ids do
      child_name = String.to_atom("#{__MODULE__}-#{shard_id}")
      Logger.info("starting #{child_name}...")

      {:ok, _} =
        DynamicSupervisor.start_child(
          name,
          {KinesisStage.ShardProducerStream, {shard_id, child_name, opts}}
        )

      child_name
    end
  end
end
