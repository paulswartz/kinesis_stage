defmodule KinesisStage.ShardProducer do
  @moduledoc """
  Producer which fetches data from a single Kinesis shard.
  """
  use GenStage

  def start_link({shard_id, name, opts}) do
    GenStage.start_link(__MODULE__, {shard_id, opts}, name: name)
  end

  defstruct [
    :stream_name,
    :shard_id,
    :iterator_type,
    :iterator,
    :ss,
    delay_ms: 1000,
    demand: 0,
    wait_ref: nil
  ]

  @impl GenStage
  def init({shard_id, opts}) do
    {:producer,
     %__MODULE__{
       stream_name: Keyword.fetch!(opts, :stream_name),
       shard_id: shard_id,
       iterator_type: Keyword.get(opts, :shard_iterator_type, :trim_horizon)
     }}
  end

  @impl GenStage
  def handle_demand(demand, state) do
    IO.inspect({:new_demand, state.shard_id, demand})
    state = %{state | demand: demand + state.demand}
    {state, events} = ensure_timer(state)
    {:noreply, events, state}
  end

  @impl GenStage
  def handle_info(:fetch, state) do
    {state, events} = fetch_events(state)
    {:noreply, events, state}
  end

  defp ensure_iterator(%{iterator: i} = state) when is_binary(i) do
    state
  end

  defp ensure_iterator(state) do
    # response = ExAws.request!(ExAws.Kinesis.get_shard_iterator(state.stream_name, state.shard_id, state.iterator_type, []))
    # iterator = Map.fetch!(response, "ShardIterator")
    arn = Application.get_env(:kinesis_stage, :consumer_arn)
    ss = KinesisStage.SubscribeToShard.new(arn, state.shard_id, state.iterator_type)
    KinesisStage.SubscribeToShard.subscribe(ss)
    # %{state | iterator: iterator}
    state
  end

  defp ensure_timer(%{wait_ref: r} = state) when not is_nil(r) do
    {state, []}
  end

  defp ensure_timer(state) do
    state = ensure_iterator(state)
    {:ok, ref} = :timer.send_interval(state.delay_ms, :fetch)
    state = %{state | wait_ref: ref}
    fetch_events(state)
  end

  defp fetch_events(%{demand: demand, iterator: it} = state) when demand > 0 and is_binary(it) do
    response =
      ExAws.request!(ExAws.Kinesis.get_records(state.iterator, limit: min(state.demand, 10_000)))

    %{
      "NextShardIterator" => iterator,
      "Records" => records
    } =
      response

    records =
      for record <- records do
        %{
          "Data" => data,
          "ApproximateArrivalTimestamp" => approximate_arrival,
          "PartitionKey" => partition_key,
          "SequenceNumber" => sequence_number
        } = record

        %KinesisRecord{
          stream_name: state.stream_name,
          shard_id: state.shard_id,
          data: :base64.decode(data),
          approximate_arrival_timestamp: approximate_arrival,
          partition_key: partition_key,
          sequence_number: sequence_number
        }
      end

    demand = max(0, state.demand - length(records))
    if records != [], do: IO.inspect({:sent, state.shard_id, length(records), remaining: demand})
    state = %{state | demand: demand, iterator: iterator}
    {state, records}
  end

  defp fetch_events(state) do
    # waiting
    {state, []}
  end
end
