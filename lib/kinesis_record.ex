defmodule KinesisRecord do
  defstruct [
    :stream_name,
    :shard_id,
    :data,
    :partition_key,
    :sequence_number,
    :approximate_arrival_timestamp
  ]
end
