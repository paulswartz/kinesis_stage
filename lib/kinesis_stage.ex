defmodule KinesisStage do
  @moduledoc """
  Documentation for `KinesisStage`.
  """
  use GenStage

  def start_link(opts) do
    start_link_opts = Keyword.take(opts, [:name])
    GenStage.start_link(__MODULE__, opts, start_link_opts)
  end

  @impl GenStage
  def init(opts) do
    children =
      opts[:stream_name]
      |> list_shards()
      |> ExAws.request!()
      |> IO.inspect(label: "list_shards")
      |> Map.get("Shards")
      |> Enum.map(&Map.get(&1, "ShardId"))
      |> Enum.uniq()
      |> KinesisStage.ShardSupervisor.create_children(opts)

    subscriptions =
      for child <- children do
        {child, min_demand: 7500, max_demand: 10_000}
      end

    {:producer_consumer, :state, subscribe_to: subscriptions}
  end

  def list_shards(stream_name) do
      ExAws.Operation.JSON.new(:kinesis, %{
        data: %{
          "StreamName" => stream_name,
          "ShardFilter" => %{
            "Type" => "AT_LATEST"
          }
        },
        headers: [
          {"x-amz-target", "Kinesis_20131202.ListShards"},
          {"content-type", "application/x-amz-json-1.1"}
        ]
      })
  end

  @impl GenStage
  def handle_events(events, _from, state) do
    {:noreply, events, state}
  end
end
