import Config

config :kinesis_stage,
  name: KinesisStage,
  stream_name: System.get_env("STREAM_NAME"),
  shard_iterator_type: :latest,
  consumer_arn: System.get_env("CONSUMER_ARN")

config :ex_aws,
  # ,
  json_codec: Jason

# access_key_id: "test",
# secret_access_key: "test"

# config :ex_aws, :kinesis,
#   scheme: "http://",
#   host: "localhost",
#   region: "us-east-1",
#   port: 4566

config :logger, backends: [:console]
