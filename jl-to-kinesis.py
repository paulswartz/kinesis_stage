#!/usr/bin/env python
"""
Write JSON lines to a Kinesis stream.

Usage:
$ python jl-to-kinesis STREAM_NAME < cloudevents.jl
"""
import itertools
import json
import sys
import time
import boto3

kinesis_client = boto3.client("kinesis")
STREAM_NAME = sys.argv[1]

def grouper(it, size):
    it = iter(it)
    while True:
        l = list(itertools.islice(it, size))
        if len(l) == 0:
            break
        yield l

def group_by_time(it):
    jsons = ((line, json.loads(line)) for line in it)
    for (_time, group) in itertools.groupby(jsons, lambda x: x[1]["time"]):
        yield [g[0] for g in group]

def write_lines(lines):
    records = [{"PartitionKey": json.loads(line)["partitionkey"], "Data": line}
               for line in lines]
    response = kinesis_client.put_records(
        StreamName=STREAM_NAME,
        Records=records)
    if response.get("FailedRecordCount") > 0:
        record_responses = zip(records, response["Records"])
        failures = [(record, response) for (record, response) in record_responses if "ErrorCode" in response]
        time.sleep(1)
        new_records = [record for (record, _) in failures]
        kinesis_client.put_records(StreamName=STREAM_NAME, Records=new_records)
    print("wrote", len(response["Records"]))


for lines in grouper(sys.stdin, 500):
    write_lines(lines)
